package com.example.RecyclerViewBegin

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class ColorViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val tv = itemView.findViewById<TextView>(R.id.color)


    fun bindTo(color: Int, onItemClickListener: ((Int) -> Unit)?) {

        itemView.setOnClickListener{
            onItemClickListener?.invoke(color)
        }


        tv.setBackgroundColor(color)
        tv.text = itemView.context.getString(R.string.template, color)
    }
}
