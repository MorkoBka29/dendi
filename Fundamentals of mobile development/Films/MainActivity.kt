package com.example.film

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import java.util.*

class MainActivity : AppCompatActivity() {
    lateinit var movies : Array<String>;
    val r = Random()
    var is_seen  = emptyArray<Int>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        movies = resources.getStringArray(R.array.movies)
        for(i in movies.indices){
            is_seen += 0
        }
    }

    fun onClick(view: android.view.View) {
        val tvTitle = findViewById<TextView>(R.id.title)
        val tvCom = findViewById<TextView>(R.id.com)
        tvCom.text = ""
        for (i in movies.indices){
            if (i == movies.size-1){
                tvCom.text = "Фильмы закончились!"
            }
            val randint = r.nextInt(movies.size)
            if (is_seen[randint] == 0){
                tvTitle.text= movies[randint]
                is_seen[randint] = 1
                break
            }
        }
    }
    fun onClearClick(view: android.view.View) {
        val tvCom = findViewById<TextView>(R.id.com)
        for(i in movies.indices){
            is_seen[i] = 0
        }
        tvCom.text = "Список сбросился!"
    }
}
