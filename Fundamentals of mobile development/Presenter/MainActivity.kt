package ru.myitschool.lab23

import android.content.res.Configuration
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import ru.myitschool.lab23.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private var binding: ActivityMainBinding? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding!!.root)
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        if(newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE){
            val iv = findViewById<ImageView>(R.id.box_image)
            iv.setImageResource(R.drawable.blue)
            setContentView(R.layout.activity_main)
        }
        if(newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
            val iv = findViewById<ImageView>(R.id.box_image)
            iv.setImageResource(R.drawable.orange)
            setContentView(R.layout.activity_main)
        }
    }
}
