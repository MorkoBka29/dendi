import java.io.FileInputStream
import java.util.*
import kotlin.collections.ArrayList

fun readFile(path: String): ArrayList<String> {
    val sc = Scanner(FileInputStream(path))
    val data = arrayListOf<String>()
    while (sc.hasNextLine())
        data.add(sc.nextLine())

    return data
}

fun main() {
    val roles = readFile("src/source/roles.txt")
    val phrases = readFile("src/source/text.txt")

    val map = mutableMapOf<String, MutableList<String>>()
    for (role in roles) {
        map[role] = mutableListOf()
    }

    for (i in 0 until phrases.size) {
        val index = phrases[i].indexOf(":")
        val role = phrases[i].substring(0, index)
        var phrasa = phrases[i].substring(index + 1, phrases[i].length)
        val num = i + 1
        phrasa = "$num)$phrasa"
        map[role]?.add(phrasa)
    }

    for (item in map) {
        println(item.key + ':')
        for (text in item.value) {
            println(text)
        }
        println()
    }
}

