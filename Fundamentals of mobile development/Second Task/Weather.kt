import java.util.*

fun main(args: Array<String>) {
    val scan = Scanner(System.`in`)
    println("Enter sequence length: ")

    val n = scan.nextInt()
    val org = DoubleArray(n)
    val newArray = DoubleArray(n)
    println("Enter sequence values: ")

    for (i in 0 until n) org[i] = scan.nextDouble()

    for (i in 1 until n - 1)
        newArray[i] = ((org[i-1] + org[i] + org[i+1])/3)
    newArray[0] = org[0]
    newArray[n - 1] = org[n - 1]

    println(org.joinToString(" "))
    println(newArray.joinToString(" "))
}
