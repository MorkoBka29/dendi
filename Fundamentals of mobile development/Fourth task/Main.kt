import java.util.*

private var board = Board(0)
private val player1 = "X"
private val player2 = "O"
private var currentPlayer = ""
private var answer = ""
fun main() {
  println("Введите размер вашей доски:")
  val scanner = Scanner(System.`in`)
  val size = Integer.parseInt(scanner.nextLine())

  board = Board(size)
  board.printBoard()

  while (!board.isGameOver) {
    takeTurns()
    println("$currentPlayer's turn")
    println("Введите номер строки:")
    var row = Integer.parseInt(scanner.nextLine())
    println("Введите номер столбца:")
    var col = Integer.parseInt(scanner.nextLine())
    board.placePiece(row - 1, col - 1, currentPlayer)
    if (board.isGameOver) {
      println("Хотите играть снова? Введите y или yes")
      var answer = scanner.nextLine()
      if (isPlayingAgain(answer)) {
        board.resetGame()
      }
    }
  }
}

fun takeTurns() {
  currentPlayer = if (player1 == currentPlayer) {
    player2
  } else {
      player1
  }
}

fun isPlayingAgain(answer: String): Boolean {
  return (answer.equals("y", ignoreCase = true)
    || answer.equals("yes", ignoreCase = true))
}
