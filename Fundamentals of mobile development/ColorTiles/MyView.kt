package com.example.colortiles

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.widget.Toast
import java.util.*

class MyView(context: Context?) : View(context) {
    private val p = Paint()
    private val r = Random()
    var layoutWidth = -1
    var layoutHeight = -1
    private val N = 4
    private val tiles = Array(N) { BooleanArray(N) { r.nextBoolean() } }
    private var rectSize = 100f
    private var pad = 10f
    private var isWin = false


    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)
        layoutWidth = right - left; layoutHeight = bottom - top
    }

    override fun onDraw(canvas: Canvas?) {
        var countRect = 0

        rectSize = (layoutWidth * 0.88 / N).toFloat()
        pad = (layoutWidth * 0.1 / N).toFloat()

        canvas?.apply {
            drawColor(Color.parseColor("#4D4D4D"))

            for (i in 0 until N) {
                for (j in 0 until N) {
                    val tile = tiles[i][j]
                    p.color = if (tile) {
                        Color.parseColor("#14939f")
                    } else {
                        Color.parseColor("#0e0e0e")
                    }
                    countRect += if (tile) 1 else 0

                    drawRoundRect(
                        j * rectSize + (j + 1f) * pad,
                        i * rectSize + (i + 1f) * pad,
                        (j + 1f) * (pad + rectSize),
                        (i + 1f) * (pad + rectSize),
                        20f, 20f, p
                    )
                }
            }
        }

        isWin = countRect == 0 || countRect == N * N
        if (isWin) Toast.makeText(context, "ПОБЕДА!", Toast.LENGTH_SHORT).show()
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        if (isWin) return false

        event?.apply {
            if (action != MotionEvent.ACTION_DOWN) return false

            var centerX: Int? = null
            var centerY: Int? = null

            for (i in 0 until N) {
                for (j in 0 until N) {
                    val left = j * rectSize + (j + 1f) * pad
                    val top = i * rectSize + (i + 1f) * pad
                    val right = (j + 1f) * (pad + rectSize)
                    val bottom = (i + 1f) * (pad + rectSize)

                    if (x in left..right && y in top..bottom) {
                        centerX = i
                        centerY = j
                        break
                    }
                }
            }

            if (centerX != null && centerY != null) {
                changeColor(centerX, centerY)
            }
        }
        invalidate()
        return true
    }

    private fun changeColor(x: Int, y: Int) {
        tiles[x][y] = !tiles[x][y]

        for (i in 0 until N) {
            tiles[x][i] = !tiles[x][i]
            tiles[i][y] = !tiles[i][y]
        }
    }
}
