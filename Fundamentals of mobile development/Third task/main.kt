import kotlin.math.*
import java.util.Scanner

fun main(args:Array<String>) {
  val scanner = Scanner(System.`in`)
  var a = readLine()!!.toInt()
  var n = arrayListOf<Int>()
  var m = arrayListOf<Int>()
  for (i in 1..a) {
    var arr = scanner.nextLine().split(" ").map(String::toInt)
    n.add(arr[0])
    m.add(arr[1])
  }

  for (q in 0..a-1) {
    var arr = Array(n[q], {Array(m[q], {0})})
    var h = n[q] * m[q]
    for (i in 0..max(n[q], m[q]) + max(n[q], m[q])) {
      for (j in 0..i) {
        if (i - j < n[q] && j < m[q] && arr[i - j][j] == 0) {
          arr[i - j][j] = h
          h--
        }
      }
    }

    for (i in arr) {
      for (j in i) {
        print(j.toString() + " ")
      }
      println()
    }
  }
}
