#include <Bounce2.h>;
#define PIN_BUTTON 2
#define PIN_LED 13
 
Bounce debouncer = Bounce();

void setup() {

  pinMode(PIN_BUTTON, INPUT_PULLUP);

  debouncer.attach(PIN_BUTTON);
  debouncer.interval(5);

  pinMode(PIN_LED, OUTPUT);

}

void loop() {
  debouncer.update();

  int value = debouncer.read();

  if ( value == LOW ) {
    digitalWrite(PIN_LED, HIGH );
  }
  else {
    digitalWrite(PIN_LED, LOW );
  }
}
