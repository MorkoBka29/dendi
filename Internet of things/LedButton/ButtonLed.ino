#define check_time 2000
#define led_pin 10
#define btn_pin 2
boolean button  = true;
boolean ledEnabled = false;      
boolean pr_flag = false;
boolean long_pr_flag = false;
unsigned long last_pr = 0;  

void setup() {
  Serial.begin(9600);
  pinMode(led_pin, OUTPUT);
  pinMode(btn_pin, INPUT_PULLUP);
}
 
void loop() {
   button = digitalRead(btn_pin);
   
   if (button == false  && pr_flag == false && millis() - last_pr > 20) {
    pr_flag = !pr_flag;
    last_pr = millis(); 
   }
   if (button == false && pr_flag == true && millis() - last_pr > 1000) {
    long_pr_flag = !long_pr_flag;
    last_pr = millis();
    for(int i=255;i>0;i--){
       analogWrite(led_pin,i);
       delay(1);
     }
    digitalWrite(led_pin, HIGH);
   }

   if (button == true && pr_flag == true && long_pr_flag == true) {
    pr_flag = !pr_flag;            
    long_pr_flag = !long_pr_flag;  
   }

   if (button == true && pr_flag == true && long_pr_flag == false) {
    pr_flag = !pr_flag;  
    ledEnabled = !ledEnabled;
    digitalWrite(led_pin, ledEnabled);
   }

}
