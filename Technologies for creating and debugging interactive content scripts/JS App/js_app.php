<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>JS-App</title>
        <script src="http://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <style type="text/css">
            #vueapp{
                width: 40%;
                margin: auto;
            }
        </style>
    </head>
    
    <body>
        <!— noindex —>
        <div id="vueapp" class="py-5 text-center">
                <p v-for="item in items" :class="{ active: item.isActive }"><strong>{{ item.name }} {{ item.fname }}</strong> {{ item.f }} {{ item.k }} {{ item.p }}</p>
            <p v-if="!formIsShow">
                <button class="btn btn-primary btn-lg btn-block" @click="showNewForm">Добавить студента</button>
            </p>
            <form v-if="formIsShow" v-on:submit="save">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <label>Имя</label>
                            <input class="form-control" type="text" v-model="newItem.name">
                        </div>
                        <div class="col-md-6 mb-3">
                            <label>Фамилия</label>
                            <input class="form-control" type="text" v-model="newItem.fname">
                        </div>
                    </div>
                    <div>
                        <label>Факультет</label>
                        <select class="custom-select d-block w-100" type="text" v-model="newItem.f">
                            <option>ФБКиИ</option>
                            <option>САФ</option>
                            <option>ИСН</option>
                            <option>ЮИ</option>
                            <option>ИМЭИ</option>
                            <option>МИЭЛ</option>
                            <option>ПИ</option>
                            <option>БПФ</option>
                            <option>ИФИЯМ</option>
                            <option>ИстФак</option>
                            <option>ХимФак</option>
                        </select>
                    </div>
                    <br>
                    <label>Курс</label>
                    <div class="d-block my-3">
                        <div class="custom-control custom-radio">
                            <input class="custom-control-input" type="radio" value="1" v-model="newItem.k" required="" checked=""><label class="custom-control-label">1</label>
                        </div>
                        <div class="custom-control custom-radio">
                            <input class="custom-control-input" type="radio" value="2" v-model="newItem.k"><label class="custom-control-label">2</label>
                        </div>
                        <div class="custom-control custom-radio">
                            <input class="custom-control-input" type="radio" value="3" v-model="newItem.k"><label class="custom-control-label">3</label>
                        </div>
                        <div class="custom-control custom-radio">
                            <input class="custom-control-input" type="radio" value="4" v-model="newItem.k"><label class="custom-control-label">4</label>
                        </div>
                    </div>
                <br><button class="btn btn-primary btn-lg btn-block" type="submit">Отправить</button>
            </div>
                
            </form>
        </div>
        <!— /noindex —>
        <script>
            let app = new Vue({
                el: '#vueapp',
                data: {
                    formIsShow: false,
                    newItem: {
                        name: '',
                        fname: '',
                        f: '',
                        k: ''
                    },
                    items: [
                        { name:'Дмитрий', fname: 'Выручаев', f: 'ФБКиИ', k: '3', isActive: true },
                        { name:'Василий', fname: 'Пупкин', f: 'САФ', k: '2', isActive: false },
                        { name:'Екатерина', fname: 'Иванова', f: 'ИФИЯМ', k: '4', isActive: false }
                    ]
                },
                methods: {
                    showNewForm: function() {
                        this.formIsShow = true;
                    },
                    save: function() {
                        let newItem = {
                            name: this.newItem.name,
                            fname: this.newItem.fname,
                            f: this.newItem.f,
                            k: this.newItem.k,
                        }
                        this.items.push(newItem);
                        this.newItem.name = '';
                        this.newItem.fname = '';
                        this.newItem.f = '';
                        this.newItem.k = '';
                        this.formIsShow = false;
                    },
                    created: function() {
                        this.formIsShow = true;
                    }
                }
            })
        </script>
</body>
</html>
