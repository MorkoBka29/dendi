import cv2
import numpy as np

cap = cv2.VideoCapture("2cv/video2.mp4")
cv2.namedWindow("Picture")
minn = np.ones((2,2))
count = 0

while cap.isOpened():
    ret, frame = cap.read()
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    _, thresh = cv2.threshold(gray, 130, 255, cv2.THRESH_BINARY)
    thresh = cv2.erode(thresh, minn, iterations=4)
    thresh = cv2.dilate(thresh, minn, iterations=2)
    contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    if len(hierarchy) > 0:
        contours = contours[2:]
    if len(contours) == 12:
        count += 1
    if count == 10:
        while True:
            cv2.putText(frame, f"My boy", (10, 40), cv2.FONT_HERSHEY_SIMPLEX, 1.2, (0, 0, 255), 2)
            cv2.imshow("Picture", frame)
            key = cv2.waitKey(10)
            if key == ord('q'):
                break
        break

    cv2.imshow("Picture", frame)
    key = cv2.waitKey(10)
    if key == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()
