enum class TokenType {
    INTEGER,
    PLUS,
    MINUS,
    MUL,
    DIV,
    LPAREN,
    RPAREN,
    ASSIGN,
    ID,
    BEGIN,
    END,
    SEMI,
    DOT,
    EOF
}
