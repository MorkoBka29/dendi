interface Node

data class Assign(
    val left: Node,
    val right: Node
) : Node {
    val token: Token = CommonTokens.ASSIGN
}

data class BinOp(
    val left: Node,
    val right: Node,
    val op: Token
) : Node

data class Block(
    val children: List<Node>
) : Node

data class Number(
    val token: Token
) : Node

data class UnaryOp(
    val token: Token,
    val expr: Node
) : Node

class NoOp : Node {
    override fun equals(other: Any?): Boolean {
        return other is NoOp
    }

    override fun hashCode(): Int {
        return javaClass.canonicalName.hashCode()
    }
}

class Var(
    token: Token
) : Node {
    val value: String = token.value!!

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Var

        if (value != other.value) return false

        return true
    }

    override fun hashCode(): Int {
        return value.hashCode()
    }
}

interface NodeVisitor {
    fun visit(node: Node): Any?
}
