import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class InterpreterTest{
    @Test
    fun testInter(){
        var interpreter: Interpreter
        var parser: Parser
        var test1 = "BEGIN\n" +
                "END."
        var test2 = "BEGIN\n" +
                "\tx:= 2 + 3 * (2 + 3);\n" +
                "        y:= 2 / 2 - 2 + 3 * ((1 + 1) + (1 + 1));\n" +
                "END."
        var test3 = "BEGIN\n" +
                "    y := 2;\n" +
                "    BEGIN\n" +
                "        a := 3;\n" +
                "        a := a;\n" +
                "        b := 10 + a + 10 * y / 4;\n" +
                "        c := a - b\n" +
                "    END;\n" +
                "    x := 11;\n" +
                "END."

        parser = Parser(Lexer(test3))
        interpreter = Interpreter(parser)

        interpreter.interpreter()

        assertEquals(mutableMapOf("y" to 2.0.toFloat(), "a" to 3.0.toFloat(), "b" to 18.0.toFloat(), "c" to (-15.0).toFloat(), "x" to 11.0.toFloat()), interpreter.variables)


        var parser2: Parser = Parser(Lexer(test2))
        var interpreter2: Interpreter = Interpreter(parser2)

        interpreter2.interpreter()
        assertEquals(mutableMapOf("x" to 17.0.toFloat(), "y" to 11.0.toFloat()), interpreter2.variables)

        var parser3: Parser = Parser(Lexer(test1))
        var interpreter3: Interpreter = Interpreter(parser3)

        interpreter3.interpreter()
        assertEquals(mutableMapOf<String, Float>(), interpreter3.variables)

    }
}
