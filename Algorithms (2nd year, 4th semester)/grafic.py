import matplotlib.pyplot as plt
import time

t = time.perf_counter()
print(f"lapsed{time.perf_counter()-t}")

data = input().split()

x = list(map(float,data[::2]))
y = list(map(float, data[1::2]))
yr = [i *4 for i in x ]

plt.plot(x,y)
plt.show()
