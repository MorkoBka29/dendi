def odd(value):
  sum = 0
  value = int(value)
  while value > 0:
    sum += value % 10
    value //= 10
  if sum % 2 == 0:
    return '0'
  else:
    return '1'
def hash(summbit):
  result = ""
  bit_odd = ""
  for i in range(8):
    for j in range(len(summbit)):
      bit_odd = bit_odd+(bin(ord(summbit[j]) >> (7 - i) & 1)[2:])
    result = result+(odd(bit_odd))
    bit_odd = ""
  return result
summbit = "asdqwerty"
print("Начальное сообщение: ")
print(summbit)
print("Хешированное сообщение: ")
print(hash(summbit))
