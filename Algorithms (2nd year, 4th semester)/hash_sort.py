def odd(value):
  s = 0
  value = int(value)
  while value > 0:
    s += value % 10
    value //= 10
  if s % 2 == 0:
    return 0
  else:
    return 1
def h(summbit):
  result = ""
  for i in range(4):
    result = result + bin(int(summbit[i]))[2:]
  if odd(result) == int(summbit[4]):
    return 0
  else:
    return 1
with open("out.txt", "r") as file:
  count = 0
  for i in range(100):
    count += h(file.readline().split())
  print((count), "%")
