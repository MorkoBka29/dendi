#include <iostream>
#include <cmath>
#include <map>

using namespace std;

int main () {
  setlocale(LC_ALL, "ru");
  map<int, int> MyMap;
  int counter = 0;

  for (int i = 0; i <= 7; i++) {
    counter = ceil(pow(10, i));
    MyMap.clear();
    for (int q = 0, v = 1; q<=counter; q++, v++) {
      MyMap.emplace(pair<int, int>(v, i));
    }
  cout << counter << " " << sizeof(MyMap) + MyMap.size()*sizeof(map<int, int>) << " ";
  } 
  return 0;
}
