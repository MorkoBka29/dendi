#include <ctime>
#include <iostream>
#include <cassert>

using namespace std;

class NDArray {
public:
  NDArray(int shape[], int fill[]) {
    shape_two = new int[2];
    for (int i = 0; i < 2; i++) {
      shape_two[i] = shape[i];
    }
    const int size = this->get_size();
    array_two = new int[size];
    for (int i = 0; i < size; i++) {
      array_two[i] = fill[i];
    }
  }
  void random()
  {
    const int size = this->get_size();
	  for (int i = 0; i < size; i++)
	  {
		  array_two[i] = rand();
	  }
  }
  void enter(int value) {
    const int size = this->get_size();
    array_two = new int[size];
      for (int i = 0; i < size; i++) {
        array_two[i] = value;
      }
  }
  NDArray(int shape[], int fill = -1) {
    shape_two = new int[2];
    for (int i = 0; i < 2; i++) {
      shape_two[i] = shape[i];
    }
    const int size = this->get_size();
    array_two = new int[size];    
    if (fill == -1) {
      array_two = new int[size];
    }
    if (fill == 0) {
      this->enter(0);
    }
    if (fill == 1) {
      this->enter(1);
    }
    if (fill == 2) {
      this->random();
      }
    }
  int get_size() {
    return shape_two[0] * shape_two[1];
  }
  int get_element(int index) {
    return array_two[index];
  }
  int get_ID(int value) {
    int index = 0;
    while (index != shape_two[0]) {
      if (array_two[index] == value) {
        return index;
      }
      index++;
    }
    return -1;
  }
  int* get_row(int index) {
    int* result = new int[shape_two[1]];
    int elem = 0;
    int i = index * shape_two[1];
    while (elem != shape_two[1]) {
      result[elem] = array_two[i];
      i++;
      elem++;
    }
    return result;
  }
  int* get_column(int index) {
    int* result = new int[shape_two[0]];
    int elem = 0;
    int i = index;
    while (elem != shape_two[0]) {
      result[elem] = array_two[i];
      i += shape_two[1];
      elem++;
    }
    return result;
  }
  void set(int x, int value) {
    array_two[x] = value;
  }
  void print() {
    int indexXY = 0;
    int indexX = 0;
    int indexY = 0;
    while (indexX != shape_two[0]) {
        cout << "[";
        while (indexY != shape_two[1]) {
            cout << " " << array_two[indexXY] << " ";
            indexY++;
            indexXY++;
        }
        cout << "]" << endl;
        indexY = 0;
        indexX++;
    }
}
NDArray Transpose()
{
  const int size = this->get_size();
	int* result = new int[size];
	int index = 0;
	for (int i = 0; i < shape_two[0]; i++)
	{
		int* row = new int[shape_two[1]];
		row = this->get_row(i);
    result [i] = row[0]; 
		index = i;
		for (int j = 0; j < shape_two[1] - 1; j++)
		{
			index += shape_two[0];
      result[index] = row[j+1];
		}
		delete[]row;
	}
	int resShape[2] = { shape_two[1] , shape_two[0] };
	NDArray res(resShape, result);
	return res;
}
NDArray matrix_mul(NDArray& other) {
  assert(shape_two[0] == other.shape_two[1] or shape_two[1] == other.shape_two[0]);
  int* row = new int[shape_two[1]];
  int* column = new int[shape_two[0]];
  int* result = new int[this->get_size()];
  int summ = 0;
  int count1 = 0;
  int index = 0;
  for (int i = 0; i < shape_two[0]; i++) {
    row = this->get_row(i);            
    while (count1 != other.shape_two[1]) {
      column = other.get_column(count1);                
      for (int j = 0; j < shape_two[1]; j++) {
        summ += row[j] * column[j];
        }
        result[index] = summ;
        summ = 0;
        count1++;
        index++;
    }
    count1 = 0;
    }
  int shape[2] = { shape_two[0], other.shape_two[1] };
  NDArray res(shape, result);
  return res;
}
friend NDArray operator+(const NDArray& self,const NDArray& other);
friend NDArray operator-(const NDArray& self,const NDArray& other);
friend NDArray operator*(const NDArray& self,const NDArray& other);
friend NDArray operator/(const NDArray& self,const NDArray& other);

private:
  int* shape_two;
  int* array_two;
};
NDArray operator+(const NDArray& self,const NDArray& other) {
  const int size = self.shape_two[0] * self.shape_two[1];
  int* result = new int[size];
  for (int i = 0; i < size; i++) {
    result[i] = self.array_two[i] + other.array_two[i];
    }
  NDArray out(self.shape_two, result);
  return out;
}
NDArray operator-(const NDArray& self,const NDArray& other) {
  const int size = self.shape_two[0] * self.shape_two[1];
  int* result = new int[size];
  for (int i = 0; i < size; i++) {
    result[i] = self.array_two[i] - other.array_two[i];
    }
  NDArray out(self.shape_two, result);
  return out;
}
NDArray operator*(const NDArray& self,const NDArray& other) {
  const int size = self.shape_two[0] * self.shape_two[1];
  int* result = new int[size];
  for (int i = 0; i < size; i++) {
    result[i] = self.array_two[i] * other.array_two[i];
    }
  NDArray out(self.shape_two, result);
  return out;
}
NDArray operator/(const NDArray& self,const NDArray& other) {
  const int size = self.shape_two[0] * self.shape_two[1];
  int* result = new int[size];
  for (int i = 0; i < size; i++) {
    result[i] = self.array_two[i] / other.array_two[i];
    }
  NDArray out(self.shape_two, result);
  return out;
}

int main() {
  setlocale(LC_ALL, "ru");
  int fill[9] = {1, 2, 3, 4, 5, 6, 7, 8, 9};
  int shape[2] = {3, 3};
  int shape_two[2] = {6, 1};
  NDArray arr(shape, fill);
  cout << "Первоначальные матрицы" << endl;
  arr.print();
  cout << endl;
  cout << "Транспонирование" << endl;
  arr.Transpose().print();
  cout << endl;
  cout << "Матричное умножение" << endl;
  NDArray drr = arr.Transpose();
  arr.matrix_mul(drr).print();
  cout << endl;
  cout << "Сложение" << endl;
  NDArray crr = arr + arr;
  crr.print();
  cout << endl;
  cout << "Вычитание" << endl;
  NDArray trr = arr - arr;
  trr.print();
  cout << endl;
  cout << "Умножение" << endl;
  NDArray brr = arr * arr;
  brr.print();
  cout << endl;
  cout << "Деление" << endl;
  NDArray zrr = arr / arr;
  zrr.print();
}
