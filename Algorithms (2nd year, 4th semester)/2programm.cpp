#define _USE_MATH_DEFINES
#include <iostream>
#include <math.h>
#include <fstream>

using namespace std;

int main() {
  setlocale(LC_ALL, "ru");
  ifstream in;
  in.open("number.txt");
  double x, y, z;
  while(in >> x) {
    in  >> y >> z;
    double angle = 2 * sin(x) * sin(y) + cos(z);
    cout << "Полученное значение: " << endl << angle * (180 / M_PI) << endl;
  }
  in.close();
}
