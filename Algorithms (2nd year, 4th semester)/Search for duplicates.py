import random
import time

def find_duplicates(hash_function):
    start_time = time.time()

    dictF = {}
    for i in range(500):
        with open("out/" + str(i) + ".txt", "r") as f:
            lines = f.readlines()
            t = ""
            for line in lines:
                a = line.split()
                for word in a:
                    t += word
            dictF.update([(hash_function(t), str(i))])
    print(time.time() - start_time, "прошло времени" + str(hash_function))
    print(500 - len(dictF), "Количество дубликатов")
    return dictF


def CRC(suggestions):
    h = 0
    for i in range(len(suggestions)):
        ki = int(ord(suggestions[i]))
        highorder = h & 0xf8000000
        h = h << 5
        h = h ^ (highorder >> 27)
        h = h ^ ki
    return h


def PJW(suggestions):
    h = 0
    for i in range(len(suggestions)):
        ki = int(ord(suggestions[i]))
        h = (h << 4) + ki
        g = h & 0xf0000000
        if g != 0:
            h = h ^ (g >> 24)
            h = h ^ g
    return h

def BUZ(suggestions):
    h = 0
    for i in range(len(suggestions)):
        ki = int(ord(suggestions[i]))
        highorder = h & 0x80000000
        h = h << 1
        h = h ^ (highorder >> 31)
        h = h ^ R(ki)
    return h

def R(code):
    random.seed(1)
    dictcode = {}
    for i in range(255):
        dictcode[i] = code * random.randint(0, 1000)
    return dictcode[code]

find_duplicates(hash)
find_duplicates(CRC)
find_duplicates(PJW)
find_duplicates(BUZ)

